#!/bin/bash

## Thanks
# https://serverfault.com/a/919212
##

set -e

if [[ -z "$DB_HOST" ]]; then
    export DB_HOST=mariadb
fi

if [[ -z "$DB_NAME" ]]; then
    export DB_NAME=castlecraft
fi

if [[ -z "$DB_USER" ]]; then
    export DB_USER=castlecraft
fi

if [[ -z "$DB_PASSWORD" ]]; then
    echo "ERROR: Environment variable DB_PASSWORD not set"
    exit 1
fi

if [[ -z "$HASH_SALT" ]]; then
    echo "ERROR: Environment variable HASH_SALT not set"
    exit 1
fi

envsubst '${DB_HOST}
  ${DB_NAME}
  ${DB_USER}
  ${DB_PASSWORD}
  ${HASH_SALT}' < /var/www/html/sites/default/settings.tmpl > /var/www/html/sites/default/settings.php

# exec runuser -u craft "$@"
exec "$@"
