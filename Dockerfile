FROM php:7.2-apache

RUN apt-get update \
    && apt-get install -y gettext-base git \
    && docker-php-ext-install pdo pdo_mysql mysqli \
    && a2enmod rewrite \
    # Install Composer
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=. --filename=composer \
    && mv composer /usr/local/bin/ \
    && apt-get remove -y git \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY . /var/www/html/
RUN mv /var/www/html/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh \
    && useradd -ms /bin/bash craft
    # && chown -R craft:craft /var/www/html/

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["apache2-foreground"]
