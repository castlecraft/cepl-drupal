## Build docker image

```shell
docker build -t registry.gitlab.com/castlecraft/cepl-drupal/drupal:latest .
```

## Setup database and files

- Place the dump of database named `castlecraft.sql` in `initdb.d` directory
- Place all uploaded files in `files` directory.
- copy `example.env` as `.env`

## Start containers

```shell
docker-compose up -d
```
